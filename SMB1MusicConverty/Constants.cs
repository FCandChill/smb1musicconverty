﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Constants
{
    public const int NULL = -1;
    public const int b_NULL = 0xFF;
    public const int Sound_Channels = 5;

    public enum Channel
    {
        Square1,
        Square2,
        Triangle,
        Noise,
        DPCM
    }


}