﻿using System;
using System.Collections.Generic;

public class ConvertToSMB1
{
    private enum Soundchannel
    {
        Square1,
        Square2,
        Triangle,
        Noise,
        DPCM
    }

    private const int
    LENGTH1 = 20,
    LENGTH2 = 16,
    LENGTH3 = 12,
    LENGTH4 = 08,
    LENGTH5 = 06,
    LENGTH6 = 04,
    LENGTH7 = 02,
    LENGTH8 = 01;

    public void Convert()
    {
        //byte b = ConvertNoteSquare1("C-4", LENGTH1);

        int SoundChannel = 0;

        foreach (List<SequenceLine[]> ll_seq in Master.Measures)
        {
            Master.Byte_Data[SoundChannel] = new List<List<byte>>();

            int MeasureNumber = 0;
            foreach (SequenceLine[] Measure in ll_seq)
            {
                int Prev_Length = 0;
                int Curr_Length = 0;
                byte Prev_Note = 0,
                    byte_Current_Note = 0,
                    Prev_Volume = 0xF,
                    Current_Volume = 0xF,
                    PrevLength = 0xF,
                    CurrLength = 0XF;

                bool First = true;

                bool EmptyMeasure = true;

                string
                    s_Prev_Note = "",
                    s_Current_Note = "";
                int Prev_instrument = -1,
                    Current_instrument = -1;


                List<Effect>
                    s_Current_SFX = null,
                    Prev_SFX = null;

                List<byte> MeasureBytes = new List<byte>();

                for (int i = 0; i < Measure.Length + 1; i++)
                {
                    if (SoundChannel == (int)Constants.Channel.DPCM)
                    {

                    }
                        Console.WriteLine(string.Format("Soundchannel: {0} Measure: {1} Line {2}", SoundChannel.ToString("X2"), MeasureNumber.ToString("X2"), i.ToString("X2")));

                    bool End = (i >= Measure.Length);

                    if (!End)
                    {
                        End = Measure[i].Note == "===";
                    }

                    if (!End && EmptyMeasure && Measure[i].Note != "")
                    {
                        EmptyMeasure = false;
                    }

                    if (!End)
                    {
                        if (!EmptyMeasure)
                        {
                            s_Current_Note = Measure[i].Note;
                            Current_instrument = Measure[i].Instrument;
                            s_Current_SFX = Measure[i].Effects;
                            Current_instrument = Measure[i].Instrument;
                        }
                    }

                    
                    if (!End && string.IsNullOrEmpty(s_Current_Note) && i != Measure.Length) //write note if last frame
                    {
                        Curr_Length++;
                    }
                    else
                    {
                        //ignore writing first line
                        if (i != 0 && !EmptyMeasure)
                        {
                            Curr_Length++;
                            bool SoundEffect = false;
                            if (Prev_SFX != null)
                            {
                                foreach (Effect e in Prev_SFX)
                                {
                                    if (e.Effect_Prefix == "R")
                                    {
                                        SoundEffect = true;
                                    }
                                }
                            }

                            if (!SoundEffect)
                            {
                                

                                if (SoundChannel == (int)Constants.Channel.Square2 || SoundChannel == (int)Constants.Channel.Triangle)
                                {
                                    CurrLength = NoteLengthCode(Curr_Length);

                                    if (PrevLength != CurrLength)
                                    {
                                        MeasureBytes.Add(NoteLengthCode(Curr_Length));
                                    }

                                    PrevLength = CurrLength;
                                }

                                byte NoteData = 0x0;
                                switch (SoundChannel)
                                {
                                    case (int)Constants.Channel.Square1:
                                        NoteData = ConvertNoteSquare1(s_Prev_Note, Curr_Length);
                                        break;
                                    case (int)Constants.Channel.Square2:
                                    case (int)Constants.Channel.Triangle:
                                        NoteData = ConvertNoteSquare2ANDTriangle(s_Prev_Note);
                                        break;
                                    case (int)Constants.Channel.Noise:
                                        NoteData = ConvertNoteNoise(s_Prev_Note, Prev_instrument, Curr_Length);
                                        break;
                                    default:
                                        throw new Exception("Invalid sound channel ID.");
                                }
                                MeasureBytes.Add(NoteData);

                                
                            }
                            else
                            {
                                MeasureBytes.Add(0x00);
                            }


                            Curr_Length = 0;
                        }

                        s_Prev_Note = s_Current_Note;
                        s_Current_Note = "";

                        Prev_instrument = Current_instrument;
                        Current_instrument = -1;

                        Prev_SFX = s_Current_SFX;
                        Prev_SFX = null;

                        if (End)
                        {
                            if (SoundChannel == (int)Constants.Channel.Square2 || SoundChannel == (int)Constants.Channel.Triangle)
                            {
                                MeasureBytes.Add(0x00);
                            }
                            break;
                        }
                    }
                }

                Master.Byte_Data[SoundChannel].Add(MeasureBytes);
                MeasureNumber++;
            }

            SoundChannel++;
        }
    }


    private byte NoteLengthCode(int blank_spaces)
    {
        byte ret = 0x0;
        switch (blank_spaces)
        {
            case LENGTH1: ret = 0x80; break;
            case LENGTH2: ret = 0x86; break;
            case LENGTH3: ret = 0x85; break;
            case LENGTH4: ret = 0x84; break;
            case LENGTH5: ret = 0x87; break;
            case LENGTH6: ret = 0x82; break;
            case LENGTH7: ret = 0x83; break;
            case LENGTH8: ret = 0x81; break;
            default: throw new Exception($"Invalid length: {blank_spaces}.");
        }
        return ret;
    }

    private byte ConvertNoteSquare1(string Note, int Length)
    {
        byte FirstNibble = 0, LastNibble = 0;

        if (!string.IsNullOrEmpty(Note))
        {
            switch (Note)
            {
                case "B#4": FirstNibble -= 0x0; LastNibble = 0xE; break;
                case "A#4": FirstNibble -= 0x0; LastNibble = 0xC; break;
                case "G-4": FirstNibble -= 0x0; LastNibble = 0xA; break;
                case "F#4": FirstNibble -= 0x0; LastNibble = 0x8; break;
                case "F-4": FirstNibble -= 0x0; LastNibble = 0x6; break;
                case "E-4": FirstNibble -= 0x0; LastNibble = 0x4; break;
                case "E#4": FirstNibble -= 0x0; LastNibble = 0x2; break;
                case "D-4": FirstNibble -= 0x0; LastNibble = 0x0; break;
                case "C#4": FirstNibble -= 0x1; LastNibble = 0xE; break;
                case "C-4": FirstNibble -= 0x1; LastNibble = 0xC; break;
                case "B-3": FirstNibble -= 0x1; LastNibble = 0xA; break;
                case "B#3": FirstNibble -= 0x1; LastNibble = 0x8; break;
                case "A-3": FirstNibble -= 0x1; LastNibble = 0x6; break;
                case "G-3": FirstNibble -= 0x1; LastNibble = 0x2; break;
                case "F#3": FirstNibble -= 0x1; LastNibble = 0x0; break;
                case "F-3": FirstNibble -= 0x2; LastNibble = 0xE; break;
                case "E-3": FirstNibble -= 0x2; LastNibble = 0xC; break;
                case "E#3": FirstNibble -= 0x2; LastNibble = 0xA; break;
                case "D-3": FirstNibble -= 0x2; LastNibble = 0x8; break;
                case "C#3": FirstNibble -= 0x2; LastNibble = 0x6; break;
                case "C-3": FirstNibble -= 0x2; LastNibble = 0x4; break;
                case "B-2": FirstNibble -= 0x2; LastNibble = 0x2; break;
                case "B#2": FirstNibble -= 0x3; LastNibble = 0x0; break;
                case "A-2": goto default;
                case "A#2": FirstNibble -= 0x3; LastNibble = 0xE; break;
                case "G-2": FirstNibble -= 0x3; LastNibble = 0xC; break;
                case "F#2": FirstNibble -= 0x3; LastNibble = 0xA; break;
                case "F-2": FirstNibble -= 0x3; LastNibble = 0x8; break;
                case "E-2": FirstNibble -= 0x3; LastNibble = 0x6; break;
                case "---": FirstNibble -= 0x3; LastNibble = 0x4; break;
                case "D-6": FirstNibble -= 0x3; LastNibble = 0x2; break;
                case "???": FirstNibble -= 0x0; LastNibble = 0x0; break; //reserved for note slides.
                default: throw new Exception($"Invalid note \"{Note}\".");
            }
            

            switch (Length)
            {
                case LENGTH1: FirstNibble += 0x3; LastNibble += 0x00; break;
                case LENGTH4: FirstNibble += 0x3; LastNibble += 0x01; break;

                case LENGTH6: FirstNibble += 0xB; LastNibble += 0x00; break;
                case LENGTH2: FirstNibble += 0xB; LastNibble += 0x01; break;

                case LENGTH8: FirstNibble += 0x7; LastNibble += 0x00; break;
                case LENGTH3: FirstNibble += 0x7; LastNibble += 0x01; break;

                case LENGTH5: FirstNibble += 0xF; LastNibble += 0x00; break;
                case LENGTH7: FirstNibble += 0xF; LastNibble += 0x01; break;
                default: throw new Exception($"Invalid length: {Length}.");
            }
        }
        else throw new Exception();

        return (byte)((FirstNibble << 4) + LastNibble);
    }

    private byte ConvertNoteSquare2ANDTriangle(string Note)
    {
        byte ByteData = 0x0;
        if (!string.IsNullOrEmpty(Note))
        {
            switch (Note)
            {
                case "G-6": ByteData = 0x58; break;
                case "E-6": ByteData = 0x56; break;
                case "D-6": ByteData = 0x02; break;
                case "C-6": ByteData = 0x54; break;
                case "B#5": ByteData = 0x52; break;
                case "A#5": ByteData = 0x50; break;
                case "G-5": ByteData = 0x4E; break;
                case "F#5": goto default;
                case "F-5": ByteData = 0x4C; break;
                case "E-5": ByteData = 0x44; break;
                case "E#5": ByteData = 0x4A; break;
                case "D-5": ByteData = 0x48; break;
                case "C#5": ByteData = 0x46; break;
                case "C-5": ByteData = 0x64; break;
                case "B-4": ByteData = 0x42; break;
                case "B#4": ByteData = 0x3E; break;
                case "A-4": ByteData = 0x40; break;
                case "A#4": ByteData = 0x3C; break;
                case "G-4": ByteData = 0x3A; break;
                case "F#4": ByteData = 0x38; break;
                case "E-4": ByteData = 0x34; break;
                case "E#4": ByteData = 0x32; break;
                case "D-4": ByteData = 0x30; break;
                case "C#4": ByteData = 0x2E; break;
                case "C-4": ByteData = 0x2C; break;
                case "B-3": ByteData = 0x2A; break;
                case "B#3": ByteData = 0x28; break;
                case "A-3": ByteData = 0x26; break;
                case "A#3": ByteData = 0x24; break;
                case "G-3": ByteData = 0x22; break;
                case "F#3": ByteData = 0x20; break;
                case "F-3": ByteData = 0x1E; break;
                case "E-3": ByteData = 0x1C; break;
                case "E#3": ByteData = 0x1A; break;
                case "D-3": ByteData = 0x18; break;
                case "C#3": ByteData = 0x16; break;
                case "C-3": ByteData = 0x14; break;
                case "B-2": ByteData = 0x12; break;
                case "B#2": ByteData = 0x10; break;
                case "A-2": ByteData = 0x62; break;
                case "A#2": ByteData = 0x0E; break;
                case "G-2": ByteData = 0x0C; break;
                case "F#2": ByteData = 0x0A; break;
                case "F-2": ByteData = 0x08; break;
                case "E-2": ByteData = 0x06; break;
                case "Eb2": ByteData = 0x60; break;
                case "D-2": ByteData = 0x5E; break;
                case "C-2": ByteData = 0x5C; break;
                /*case "G-2": ByteData = 0x5A; break;*/
                case "---": ByteData = 0x04; break;
                default: throw new Exception($"Invalid note \"{Note}\".");
            }
        }
        else throw new Exception(Note + " is not a valid note."); //???

        return ByteData;
    }
   private enum Noise
    {
        Snare = 3,
        Kick = 4,
        Hihat = 5
    }

    private byte ConvertNoteNoise(string Note, int InstrumentNo, int Length)
    {
        byte Data = 0;
        if (!string.IsNullOrEmpty(Note))
        {
            if (Note != "---")
            {
                switch (InstrumentNo)
                {
                    case (int)Noise.Snare: Data = 0x30; break;
                    case (int)Noise.Kick: Data = 0x20; break;
                    case (int)Noise.Hihat: Data = 0x10; break;
                    default: throw new Exception("Invalid instrument ID for drum.");
                }
            }
            else
            {
                Data = 0x04; //Rest
            }

            switch (Length)
            {
                case LENGTH1: Data += 0x00; break;
                case LENGTH4: Data += 0x01; break;

                case LENGTH6: Data += 0x80; break;
                case LENGTH2: Data += 0x81; break;

                case LENGTH8: Data += 0x40; break;
                case LENGTH3: Data += 0x41; break;
                    
                case LENGTH5: Data += 0xC1; break;
                case LENGTH7: Data += 0xC0; break;
                default: throw new Exception($"Invalid length: {Length}.");
            }
        }
        else throw new Exception();

        return Data;
    }
}
