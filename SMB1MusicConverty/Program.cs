﻿using System;

namespace SMB1MusicConverty
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1) 
            {
                ReadFtmTXT.Read(args[0]);
                ConvertToSMB1 smb1 = new ConvertToSMB1();
                smb1.Convert();
                Master.WriteFiles();
            } 
            else
            {
                Console.WriteLine("The only argument should be the path to the famitracker .txt file.");
            }
        }
    }
}