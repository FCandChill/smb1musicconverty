# SMB1MusicConverty

This program help ROM hackers compose music for Super Mario Bros. for the NES. This program takes TXT files exported from Famitracker and converts it to binary files.

### How it works
The program reads the text file and stores all the data in a data structure. Then it will read every line of the data structure and convert it to binary data.

The program stores notes and other things in two variables. One for the previously read and one for the currently read. To get the length of a note, I have to read ahead until I hit another note. When it hits another note, I can calculate the length of the note.

### How to use

The program is a commandline utility. You have to specify one argument: the path to your TXT file.

The projects comes with a premade famitracker file, "SMB1.ftm". Use this as a template.

### FAQ

**Q:** I get a length error. What gives?

**A:** The Super Mario Bros. sound engine only supports notes of certain lengths.

Here's the lengths: 20, 16, 12, 08, 06, 04, 02, and 01.

These lengths correspond to the blank spaces in FamiTracker.

**Q:** I get a note error. What gives?

**A:** The Super Mario Bros. sound engine is limited to what notes you can use.

Take a look at ConvertToSMB1.cs to see what notes are supported.

For more details on what's supported by the sound engine, check the source code, or check out [Dr. Floppy's music documentation](https://www.romhacking.net/documents/390/).

The program logs where the converter is reading. It gives you a good area where to troubleshoot (the channel, measure, and line number). For example, if you get the following error:

Soundchannel: 00 Measure: 00 Line 2C

Invalid note A-2

You know that you can't use A-2 as a note. It's in the first sound channel (pulse 1). By measure I mean the pattern that's indicated in the top left. Line 2C is the line number, the numbers you see on the bottom left side. Line number 2C probably has a note that is supported by the sound engine, but what's above it? A-2. According to Dr. Floppy's sound engine, this note is not supported by the sound engine without hacking.

**Q:** Alright, I successfully ran the utlity. What do I do with these .bin files?

**A:** Each .bin file corresponds to a different measure for a sound channel. This tool is meant to aid in the music insertion process, not to fully automate it.
If you're working off the rom, you'll have to copy and paste it into a hex editor and maybe repoint some pointers. For more information on this, check out [Dr. Floppy's music documentation](https://www.romhacking.net/documents/390/).

If you're operating on a disassembly, you'll have to convert the hex binaries to a format that supports .db. To do this, I suggest copying the hex data from a hex editor and pasting them in Notepad++. Then do the following find and replace with regex enabled (the below assumes there's no spaces in the data):

Find: \[0-9A-Z\]\[0-9A-Z\]

Replace: , \$$&

It's not perfect, but it will add commas between all the bytes. If enough people want me to dump the music data into the .db format, I will.