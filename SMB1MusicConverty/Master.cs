﻿using System.Collections.Generic;
using System.IO;

public static class Master
{
    public static List<SequenceLine[]>[] Measures = new List<SequenceLine[]>[Constants.Sound_Channels];
    public static int Speed;

    public static List<List<byte>>[] Byte_Data = new List<List<byte>>[Constants.Sound_Channels];
    //string 
    public static void WriteFiles()
    {
        for(int Channel = 0; Channel < Byte_Data.Length; Channel++)
        {
            int MeasureNo = 0;
            
            foreach(List<byte> Byte in Byte_Data[Channel])
            {
                string FileName = string.Format("Soundchannel- {0} Measure- {1}.bin", Channel.ToString("X2"), MeasureNo.ToString("X2"));

                File.Delete(FileName);

                if (Byte.Count > 0)
                {
                    File.WriteAllBytes(FileName, Byte.ToArray());
                }
                MeasureNo++;
            }
        }
    }
}
